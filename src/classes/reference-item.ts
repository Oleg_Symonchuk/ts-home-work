import {timeout} from '../decorators';
export abstract class ReferenceItem {
// title: string;
// year: number;

#id: number;
private _publisher: string;

static department: string = 'Classical Literature';

// constructor(newTitle: string, newYear: number) {
//     console.log('Creating a new referenceItem...');
//
//     this.title = newTitle;
//     this.year = newYear;
// }

constructor(id: number, public title: string, protected year: number) {
    console.log('Creating a new referenceItem...');
    this.#id = id;
}

getID(): number {
    return this.#id;
}

@timeout(5000)
printItem(): void {
    console.log(`${this.title} was published ${this.year}`);
    console.log(`Department ${ReferenceItem.department}`);
}

get publisher(): string {
    // eslint-disable-next-line no-underscore-dangle
    return this._publisher.toUpperCase();
}

set publisher(newPublisher: string) {
    // eslint-disable-next-line no-underscore-dangle
    this._publisher = newPublisher;
}

abstract printCitation(): void;
}