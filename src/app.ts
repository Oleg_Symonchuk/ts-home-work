/* eslint-disable @typescript-eslint/quotes */
/* eslint-disable no-redeclare */

import { Library, RefBook, Shelf, UniversityLibrarian, ReferenceItem } from './classes';
import { Category } from './enums';
import { Book, Logger, Magazine, Author, Librarian, ShelfItem } from './intefaces';
import {
    bookTitleTransform,
    calcTotalPages,
    checkoutBooks,
    createCustomer,
    createCustomerID,
    getAllBooks,
    getBookAuthorByIndex,
    getBookByID,
    getBookTitlesByCategory,
    getProperty,
    getTitles,
    logBookTitles,
    logCategorySearch,
    logFirstAvailable,
    logSearchResults,
    printBook,
    showHello,
} from './functions';
import { purge, getBooksByCategory, getBooksByCategoryPromise } from './functions';
import { BookRequiredFields, UpdatedBook, СreateCustomerFunctionType, PersonBook } from './types';
import Encyclopedia from './classes/encyclopedia';

showHello('greeting', 'TypeScript');

/**
 * ===================================================
 *              02. Types Basics
 * ===================================================
 */

//  ---------------------------------------------------
//  Task 02.01. Basic Types
//  ---------------------------------------------------
// console.log('getAllBooks:', getAllBooks());

const ab = getAllBooks();
logFirstAvailable(getAllBooks());

const titles = getBookTitlesByCategory(Category.CSS);
logBookTitles(titles);

console.log('getBookAuthorByIndex(2): ', getBookAuthorByIndex(2));
console.log('calcTotalPages:', calcTotalPages());

/**
 * ===================================================
 *              03. Functions
 * ===================================================
 */

// ---------------------------------------------------
// Task 03.01. Function Type
// ---------------------------------------------------
const myID: string = createCustomerID('Ann', 10);
console.log('myID: ', myID);

let idGenerator: (p1: string, p2: number) => string;

idGenerator = (name: string, id: number) => `${id} - ${name}`;

idGenerator = createCustomerID;
console.log("idGenerator('Boris', 30): ", idGenerator('Boris', 30));

// ---------------------------------------------------
//  Task 03.02. Optional, Default and Rest Parameters
// ---------------------------------------------------

createCustomer('Anna');
createCustomer('Anna', 30);
createCustomer('Anna', 30, 'Kyiv');

console.log('getBookTitlesByCategory(): ', getBookTitlesByCategory());
logFirstAvailable();

console.log(getBookByID(1));

const myBooks = checkoutBooks('Ann', 1, 2, 4);
console.log('myBooks: ', myBooks);

// ---------------------------------------------------
//  Task 03.03. Function Overloading
// ---------------------------------------------------

const checkedOutBooks = getTitles(false);

console.log('checkedOutBooks: ', checkedOutBooks);

console.log('getTitles(9, true): ', getTitles(9, true));
console.log('getTitles(1, true): ', getTitles(1, true));
console.log('getTitles(false): ', getTitles(false));
console.log("getTitles('Lea Verou'): ", getTitles('Lea Verou'));

// ---------------------------------------------------
//  Task 03.04. Assertion Functions
// ---------------------------------------------------

console.log("assertStringValue('test')", bookTitleTransform('some book title'));
// console.log('assertStringValue(111)', bookTitleTransform(111));

// ---------------------------------------------------
//  Task 04.01. Defining an Interface
// ---------------------------------------------------

// Task 04.01
const myBook: Book = {
    id: 5,
    title: 'Colors, Backgrounds, and Gradients',
    author: 'Eric A. Meyer',
    available: true,
    category: Category.CSS,
    // year: 2015,
    // copies: 3,
    pages: 200,
    markDamaged: (reason: string) => `Damaged: ${reason}`,
};

printBook(myBook);
console.log(myBook.markDamaged('missing back cover'));

// ---------------------------------------------------
//  Task 04.02. Defining an Interface for Function Types
// ---------------------------------------------------

let logDamage: Logger = (reason: string) => `Damaged ${reason}`;
logDamage('missing back cover');
const someLogDamage = (reason: string): void => {};

logDamage = someLogDamage;
logDamage('Some reason');
someLogDamage('Some other reasons');

// ---------------------------------------------------
//  Task 04.03. Extending Interface
// ---------------------------------------------------

const favoriteAuthor: Author = {
    name: 'Anna',
    email: 'anna@gmail.com',
    numBooksPublished: 3,
};

const favoriteLibrarian: Librarian = {
    name: 'Boris',
    email: 'boris@gmail.com',
    department: 'Classical literature',
    assistCustomer: (custName: string) => console.log(`Customer ${custName}`),
};

// ---------------------------------------------------
//  Task 04.04. Optional Chaining
// ---------------------------------------------------

const offer: any = {
    book: {
        title: 'Essential TypeScript',
    },
};

console.log(offer?.magazine);
console.log(offer?.magazine?.getTitle?.());
console.log(offer?.book?.getTitle?.());
console.log(offer?.book?.authors?.[0]);

// ---------------------------------------------------
//  Task 04.05. Keyof Operator
// ---------------------------------------------------

console.log(getProperty(myBook, 'title'));
console.log(getProperty(myBook, 'markDamaged'));
// console.log(getProperty(myBook, 'isBook'));

// ---------------------------------------------------
//  Task 05.01. Creating and Using Classes
// ---------------------------------------------------

// const ref: ReferenceItem = new ReferenceItem('TypeScript', 2021);
// const ref: ReferenceItem = new ReferenceItem(123, 'TypeScript', 2021);

// ref.printItem();
// ref.publisher = 'Veselka';
// console.log(ref.publisher);
// console.log(ref);
// console.log(ref.getID());

// ---------------------------------------------------
//  Task 05.02. Extending Classes
// ---------------------------------------------------

const refBook = new RefBook(1, 'typeScript', 2021, 3);
console.log(refBook);
refBook.printItem();

// ---------------------------------------------------
//  Task 05.03. Creating Abstract Classes
// ---------------------------------------------------

const refBook1 = new Encyclopedia(1, 'typeScript', 2021, 3);
console.log(refBook1);
refBook1.printCitation();

// ---------------------------------------------------
//  Task 05.04. Interfaces for Class Types
// ---------------------------------------------------

const favoriteLibrarian1: Librarian = new UniversityLibrarian();
favoriteLibrarian1.name = 'Anna';
favoriteLibrarian1.assistCustomer('Boris');

// ---------------------------------------------------
//  Task 05.05. Intersection and Union Types
// ---------------------------------------------------

const personBook: PersonBook = {
    id: 1,
    name: 'Anna',
    author: 'Anna',
    available: false,
    category: Category.CSS,
    email: 'some@email.com',
    title: 'TypeScript',
};
console.log(personBook);

// ---------------------------------------------------
// Task 06.01. Using Namespaces
// ---------------------------------------------------

// Implemented in NamespaceDemo folder

// ---------------------------------------------------
// Task 06.05. Dynamic Import Expression
// ---------------------------------------------------
if (true) {
    import('./classes').then(module => {
        console.log(new module.Reader());
    });
}

// ---------------------------------------------------
// Task 06.06. Type-Only Imports and Exports
// ---------------------------------------------------

let lib: Library;
// lib = new Library();
lib = {
    Id: 1,
    name: 'Anna',
    address: 'Kyiv',
};

console.log(lib);

// ---------------------------------------------------
// Task 07.01. Generic Functions
// ---------------------------------------------------

const inventory: Book[] = [
    { id: 10, title: 'The C Programming Language', author: 'K & R', available: true, category: Category.Software },
    { id: 11, title: 'Code Complete', author: 'Steve McConnell', available: true, category: Category.Software },
    { id: 12, title: '8-Bit Graphics with Cobol', author: 'A. B.', available: true, category: Category.Software },
    { id: 13, title: 'Cool autoexec.bat Scripts!', author: 'C. D.', available: true, category: Category.Software },
];
// const result: Book[] = purge(inventory);
const result: Book[] = purge<Book>(inventory);
// const result: number = purge(inventory);
console.log(result);

// ---------------------------------------------------
// Task 07.02. Generic Interfaces and Classes
// ---------------------------------------------------

const bookShelf = new Shelf<Book>();
inventory.forEach(book => bookShelf.add(book));
console.log(bookShelf.getFirst());

const magazines: Magazine[] = [
    { title: 'Programming Language Monthly', publisher: 'Code Mags' },
    { title: 'Literary Fiction Quarterly', publisher: 'College Press' },
    { title: 'Five Points', publisher: 'GSU' },
];

const magazineShef = new Shelf<Magazine>();
console.log('magazines', magazines);

magazines.forEach(magazine => magazineShef.add(magazine));
console.log(magazineShef.getFirst());

// ---------------------------------------------------
// Task 07.03. Generic Constraints
// ---------------------------------------------------
magazineShef.printTitles();
console.log(magazineShef.find('Five Points'));
console.log(getProperty(magazines[0], 'title'));
// console.log(getProperty<Magazine, 'title'>(magazines[0], 'title'));

// ---------------------------------------------------
// Task 07.04. Utility Types
// ---------------------------------------------------

const book: BookRequiredFields = {
    author: 'Anna',
    available: false,
    category: Category.CSS,
    id: 1,
    markDamaged: null,
    pages: 200,
    title: 'Unknown',
};
console.log(book);

const b: UpdatedBook = {
    id: 1,
    author: 'Denis',
};
console.log(b);

const params: Parameters<СreateCustomerFunctionType> = ['Anna'];
createCustomer(...params);
console.log(params);

// ---------------------------------------------------
// Task 08.01. Class Decorators (sealed)
// Task 08.02. Class Decorators that replace constructor functions (logger)
// ---------------------------------------------------
const fLibrarian = new UniversityLibrarian();
fLibrarian.name = 'Anna';
console.log(fLibrarian);
fLibrarian['printLibrarian']();
// fLibrarian.printLibrarian();

// ---------------------------------------------------
// Task 08.03. Method Decorator (writable)
// ---------------------------------------------------
const fLibrarian1 = new UniversityLibrarian();
fLibrarian1.assistFaculty = null;
// fLibrarian1.teachCommunity = null; // Uncaught TypeError: Cannot assign to read only property 'teachCommunity' of object '#<UniversityLibrarian>'

// ---------------------------------------------------
// Task 08.04. Method Decorator (timeout)
// ---------------------------------------------------
const e = new RefBook(1, 'Unknown', 2021, 2);
e.printItem();

// ---------------------------------------------------
// Task 08.05. Parameter Decorator (logParameter)
// Task 08.06. Property Decorator
// ---------------------------------------------------
const l = new UniversityLibrarian();
l.name = 'Anna';
console.log(l.name);
l.assistCustomer('Leo');
console.log(l);

// ---------------------------------------------------
// Task 08.07. Accessor Decorator
// ---------------------------------------------------
const rb = new RefBook(1, 'Unknown', 2021, 2);
// rb.copies = -10; // Uncaught Error: Invalid value
// rb.copies = 0; // Uncaught Error: Invalid value
// rb.copies = 4.5; // Uncaught Error: Invalid value
rb.copies = 5;

// ---------------------------------------------------
// Task 09.01. Callback Functions
// ---------------------------------------------------
console.log('Start');
getBooksByCategory(Category.Javascript, logCategorySearch);
getBooksByCategory(Category.Software, logCategorySearch);
console.log('Finish');

// ---------------------------------------------------
// Task 09.02. Promises
// ---------------------------------------------------
console.log('Start');
getBooksByCategoryPromise(Category.Javascript)
    .then(titles => {
        console.log(titles);
        return titles.length;
    })
    .then(numb => console.log(numb))
    .catch(err => console.log(err));
getBooksByCategoryPromise(Category.Software)
    .then(titles => {
        console.log(titles);
        return titles.length;
    })
    .then(numb => console.log(numb))
    .catch(err => console.log(err));
console.log('Finish');

// ---------------------------------------------------
// Task 09.03. Async Functions
// ---------------------------------------------------
console.log('Start');
logSearchResults(Category.Javascript);
logSearchResults(Category.Software).catch(err => console.log(err));
console.log('Finish');

/**
 * ===================================================
 *              Home work
 * ===================================================
 */

//  ---------------------------------------------------
//  Home work 01. Utility Types
//
//  1) Create an aliace SimpleBook by picking the following
//  properties from type Book: id, title, author.
//  2) Create a variable of SimpleBook type and assign
//  a proper object to it.
//  ---------------------------------------------------

type SimpleBook = Pick<Book, 'id' | 'title' | 'author'>;
const sBook: SimpleBook = {
    id: 33,
    author: 'Andrea Chiarelli',
    title: 'Mastering JavaScript Object-Oriented Programming',
};
console.log('sBook: ', sBook);

//  ---------------------------------------------------
//  Home work 02. Utility Types
//
//  1) Create type SellerCategory = 'best' | 'worst'
//  2) Create a variable of an object type whose property
//  keys are SellerCategory and whose property values
//  are SimpleBook by using Record utility.
//  ---------------------------------------------------

type SellerCategory = 'best' | 'worst';
const ratedBooks: Record<SellerCategory, SimpleBook> = {
    best: {
        id: 58,
        author: 'Evan Burchard',
        title: 'Refactoring JavaScript',
    },
    worst: {
        id: 6,
        author: 'Liang Yuxian Eugene',
        title: 'JavaScript Testing',
    },
};
console.log('ratedBooks: ', ratedBooks);

//  ---------------------------------------------------
//  Home work 02. Generics
//
//  1) Create a factory createInstance using generics
//  ---------------------------------------------------

type VehicleTypes = 'Sedan' | 'Coupe' | 'Hatchback' | 'Station vagon' | 'SUV';
enum VehicleBrands {
    Audi = 'Audi',
    'BMW' = 'BMW',
    Mercedes = 'Mercedes',
    Volvo = 'Volvo',
}
type VehicleParams = { brend: VehicleBrands; type: VehicleTypes };
type ModelParams = VehicleParams & { name: string; year: number };

class Vehicle {
    brend: VehicleBrands;
    type: VehicleTypes;

    constructor(params: VehicleParams) {
        this.brend = params.brend;
        this.type = params.type;
        console.log(`Creating Vehicle: ${this.brend}, type: ${this.type}`);
    }

    printMe() {
        console.log(`I'm Vehicle: ${this.brend}, type: ${this.type}`);
    }
}
class Model extends Vehicle {
    name: string;
    year: number;

    constructor(params: ModelParams) {
        super(params);

        this.name = params.name;
        this.year = params.year;

        console.log(`Creating Vehicle Model: ${this.name}, ${this.year}, ${this.brend}, type: ${this.type}`);
    }

    printMe() {
        console.log(`I'm Vehicle Model: ${this.name}, ${this.year}, ${this.brend}, type: ${this.type}`);
    }
}

type nParams<T, P> = new (params: P) => T;

function createInstance<T extends Vehicle, P>(c: nParams<T, P>, args: P): T {
    return new c(args);
}
const volvoArgs: VehicleParams = { brend: VehicleBrands.Volvo, type: 'SUV' };
const volvoInst = createInstance(Vehicle, volvoArgs);
volvoInst.printMe();

const audiInst = createInstance(Model, { name: 'RS6', year: 2021, brend: VehicleBrands.Audi, type: 'Sedan' });
audiInst.printMe();
