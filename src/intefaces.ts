
import {Category} from './enums';

interface DamageLogger {
    (reason: string): void;
}

interface Book {
    id: number;
    title: string;
    author: string;
    available: boolean;
    category: Category;
    pages?: number;
    // markDamaged?: (reason: string) => {};
    markDamaged?: DamageLogger;
}

interface Person {
    name: string;
    email: string;
}

interface Author extends Person {
    numBooksPublished: number;
}

interface Librarian extends Person {
    department: string;
    // assistCustomer: DamageLogger;
    assistCustomer: (custName: string) => void;
}


interface A {
    a: string;
}

interface B extends Partial<A>{
    b: string;
}

interface C {
    a: string;
    c: string;
}

interface D extends A, C {
    d: number;
}

const c: B = {
    a: 'a',
    b: 'b',
    // c: 'c',
};

interface G<T> {
    // m1: (a: T) => T;
    // m2: (a: T, b: number) => T;
    [index: string]: (a: T) => T;
}
let g: G<number>;
g = {
    m1(a: number) {
        return a;
    },

    m2(a: number) {
        return a;
    },
};

interface Magazine {
    title: string;
    publisher: string;
}

interface ShelfItem {
    title: string;
}

interface LibMgrCallback {
    (err: Error, titles: string[]): void;
}

export {Book, DamageLogger as Logger, Magazine, Person, Author, Librarian, ShelfItem, LibMgrCallback};